<?php

session_start();
require 'app.php';
require_once __DIR__ . '/vendor/autoload.php';
use PayPal\Api\Payer;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Amount;
use PayPal\Api\Transaction;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Payment;

    if (!isset($_GET['token']))
    {
        header("location:http://virtual.thebiznation.com/#sectionRegister");
    }
    if(!isset($_GET['token'],$_GET['price'],$_GET['course_id'] )){

        header("Location:http://virtual.thebiznation.com/");
    }

/**
 * @var string
 */

$token = $_GET['token'];

/**
 * @var string
 */

$course_name = "Cursos The Biz Nation id:".$_GET['course_id'];

/**
 * @var int
 */

$price = $_GET['price'];

/**
 * @var int
 */

$Course_id = $_GET['course_id'];

/**
 * @var string
 */

$_SESSION["token"] = $token;
$_SESSION["course_id"] = $Course_id;

//Paypal

    $product = $course_name;
    $im = $price;

    $payer = new Payer();
    $payer->SetPaymentMethod('paypal');

    $item = new Item();
    $item->setName($product)
        ->setCurrency('USD')
        ->setQuantity(1)
        ->setPrice($im);

    $itemList = new ItemList();
    $itemList->setItems([$item]);

    $amount = new Amount();
    $amount->setCurrency('USD')->setTotal($im);

    $transaction = new Transaction();
    $transaction->setAmount($amount)->setItemList($itemList)
    ->setDescription('¡THE BIZ NATION Aprende de manera fácil!')
    ->setInvoiceNumber(uniqid());

    $redirectUrls = new RedirectUrls();
    $redirectUrls->setReturnUrl(SITE_URL. 'response.php?success=true')->setCancelUrl(SITE_URL.'response.php?success=false');

    $payment = new Payment();
    $payment->setIntent('sale')
    ->setPayer($payer)
    ->setRedirectUrls($redirectUrls)
    ->setTransactions([$transaction]);

    try {
        $payment->create($paypal);
        } catch (Exception $e) {
        header("Location:http://virtual.thebiznation.com/");
        }
    $approvalUrl = $payment->getApprovalLink();

    header("Location:{$approvalUrl}");

    
