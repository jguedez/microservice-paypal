<?php
require_once __DIR__ . '/vendor/autoload.php';
require "app.php";

use Paypal\Api\Payment;
use Paypal\Api\PaymentExecution;

if (!isset($_GET['success'], $_GET['paymentId'],$_GET['Payer_ID'] ))
{
   header('location:https://www.facebook.com/ ');
}
if((bool)$_GET['success']=== false){
    header('location:https://www.facebook.com/ ');
}

$paymentId = $_GET['paymentId'];
$payerId = $_GET['payerID'];

$payment = Payment::get($paymentId,$paypal);

$execute = new PaymentExecution();
$execute->setPayerId($payerId);

try{
    $result = $payment->execute($execute, $paypal);
}catch (Exception $e){
    $data = json_decode($e->getData());
    echo $data->message;
    die();
}

echo 'Payment made. Thanks!';

?>