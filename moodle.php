<?php

/**
* @param $token
* @return string
*/

function getUserId($token)
{
    /**
     * @var string
     */
    $endPoint = 'https://moodle.thebiznation.com/webservice/rest/server.php';

    /**
     * @var string
     */
    $url = $endPoint."?wstoken=".$token."&wsfunction=core_webservice_get_site_info&moodlewsrestformat=json";

    $result = file_get_contents($url);
    $json = json_decode($result, true);
    $id= $json['userid'];
    return $id;


}

/**
* @var string
*/

 //$userId = getUserId('25e39dad94cf28dddf679cf3471361bb');

/**
* @param $id
 * @param $Course_id
* @return string
*/
function updateUser($id, $Course_id)
{
    /**
     * @var string
     */

    $token = '729dd6d397e1eee98fc302797faf6ff3';

    /**
     * @var string
     */

    $domainname = 'http://moodle.thebiznation.com/';

    /**
     * @var string
     */

    $restformat = 'json';

    /**
     * @var string
     */

    $restformat = ($restformat == 'json')?'&moodlewsrestformat=' . $restformat:'';


    header('Content-Type: text/plain');
    require_once('curl.php');
    $curl = new curl;
    $functionname = 'enrol_manual_enrol_users';
    $params = array(

        'enrolments[0][roleid]' => 5,
        'enrolments[0][userid]' => $id,
        'enrolments[0][courseid]' => $Course_id,
    );
    $serverurl = $domainname . '/webservice/rest/server.php'. '?wstoken=' . $token . '&wsfunction='.$functionname;
    $get = $curl->post($serverurl . $restformat, $params);
    $resp = json_decode($get);

    return $resp;

}

//URL https://moodle.thebiznation.com/webservice/rest/server.php?wstoken=82c79623242a43759c7e79563bddba70&wsfunction=enrol_manual_enrol_users&moodlewsrestformat=json&enrolments[0][roleid]=5&enrolments[0][userid]=14&enrolments[0][courseid]=3

