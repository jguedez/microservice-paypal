<?php

session_start();
require_once __DIR__ . '/vendor/autoload.php';
require "app.php";
require_once 'moodle.php';
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;

/**
 *  @var int
 */
$Course_id = $_SESSION["course_id"];


/**
 *  @var string
 */
$token = $_SESSION["token"];

/*
 * @param int
 * @return string
 */

function rand_string( $length ) {

    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    $size = strlen( $chars );
    for( $i = 0; $i < $length; $i++ ) {
        $str .= $chars[ rand( 0, $size - 1 ) ];
    }

    return $str;
}


/*
 * @var string
 */
$code = rand_string(30);

if (!isset($_GET['success'], $_GET['paymentId'],$_GET['PayerID'] ))
{
    header("Location:http://virtual.thebiznation.com/courses/$Course_id?paymentStatus=false&code=$code&access=$token");
}
if((bool)$_GET['success'] === false){
    header("Location:http://virtual.thebiznation.com/courses/$Course_id?paymentStatus=false&code=$code&access=$token");
}

/**
 *  @var int
 */
$paymentId = $_GET['paymentId'];

/**
 *  @var int
 */
$payerId = $_GET['PayerID'];

/*
 * @return Paypment
 */
$payment = Payment::get($paymentId,$paypal);

$execute = new PaymentExecution();
$execute->setPayerId($payerId);

try{
    $result = $payment->execute($execute,$paypal);
}catch (Exception $e){
    header("Location:http://virtual.thebiznation.com/courses/$Course_id?paymentStatus=false&code=$code&access=$token");
}

/**
 *  @var string
 */
$userId = getUserId($token);

updateUser($userId,$Course_id);

$value = $paymentId;

header("Location:http://virtual.thebiznation.com/courses/$Course_id?paymentStatus=true&code=$code&access=$token");